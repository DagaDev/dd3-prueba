import { Injectable } from '@nestjs/common';
import * as bcrypt from 'bcrypt';

const SALTOROUNDS = 8;

@Injectable()
export class CryptService{
    constructor(){}
    

    async hash(password: string){
        return bcrypt.hash(password, SALTOROUNDS);
    }

    async compare(password: string, userPaasword: string){
        return bcrypt.compare(password, userPaasword);
    }
}