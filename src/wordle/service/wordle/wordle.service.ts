import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { CurrentWord } from 'src/entities/currentword.entity';
import { UsedWords } from 'src/entities/usedwords.entity';
import { UserWord } from 'src/entities/userword.entity';
import { UserDto } from 'src/users/dtos/user.dto';
import { WordUserDto } from 'src/wordle/dtos/worduser.dto';
import { Repository } from 'typeorm';
var cron = require('node-cron');
var fs = require('fs').promises

@Injectable()
export class WordleService {
    constructor(
        @InjectRepository(CurrentWord) private readonly currentWordRepository: Repository<CurrentWord>,
        @InjectRepository(UsedWords) private readonly usedWordsRepository: Repository<UsedWords>,
        @InjectRepository(UserWord) private readonly userWord: Repository<UserWord>
    ){
    }
    private listWords: string[];
    
   
    public cronNode(){
        let _this = this;
        cron.schedule("0 */5 * * * *", function() {
            _this.initial();
        });
    }
    public async initial(){
        let word: string = await this.selectWord();
        await this.saveCurrentWord(word);
    }

    private async saveCurrentWord(word: string){
        await this.currentWordRepository.clear();
        await this.currentWordRepository.save({currentWord: word});
        await this.usedWordsRepository.save({usedWord: word});
    }

    public async getListWords():Promise<string>{
        let data: string = await fs.readFile(`${process.cwd()}/diccionario.txt`, 'utf8');
        this.listWords = data.split(/\r?\n/);
        return 'success';
    }

    private async selectWord(): Promise<string>{
        let random: number = Math.floor(Math.random() * this.listWords.length);
        let randomWord: string = this.listWords[random];
        if(randomWord.length !== 5)
            randomWord =  await this.selectWord();
        let usedWord = await this.usedWordsRepository.findOne({where: {usedWord: randomWord}})
        if(usedWord)
            randomWord =  await this.selectWord();
        
        return randomWord; 
    }

    public async userSelected(body: WordUserDto, userId: number){
        let {userWord} = body;
        let currentWordFind = await this.currentWordRepository.find();
        if(currentWordFind.length === 0)
            throw new HttpException('There is no current word', HttpStatus.BAD_REQUEST)

        let currentWord = currentWordFind[0].currentWord;

        let userWordAttempts = await this.userWord.findOne({where: { userId, word: currentWord}});
        if(!userWordAttempts)
            await this.userWord.save({userId: userId, attempts: 1, word: currentWord})
        else{
            let {attempts, win} = userWordAttempts;
            if(win)
                throw new HttpException('The user has already won', HttpStatus.BAD_REQUEST)

            if(attempts >= 5)
                throw new HttpException('Limit of attempts reached', HttpStatus.BAD_REQUEST)

            await this.userWord.update({userId: userId, word: currentWord}, {attempts: attempts+=1})
        }


        

        let userWordSplit = userWord.split('');
        let currentWordSplit = currentWord.split('');
        let response = {
            userWord: userWord,
            result: []
        };

        for (let index = 0; index < userWordSplit.length; index++) {
            let elementUser = userWordSplit[index];
            let elementCurrent = currentWordSplit[index];
            if(elementUser === elementCurrent)
                response.result.push({letter: elementUser,value: 1})
            else
            if(currentWord.includes(elementUser))
                response.result.push({letter: elementUser,value: 2})
            else
                response.result.push({letter: elementUser,value: 3})

        }
        if(response.result.every(value => value.value === 1)){
            await this.userWord.update({userId: userId, word: currentWord}, {win: true})
        }
        return response;

    }
}
