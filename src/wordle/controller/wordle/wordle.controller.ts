import { Body, Controller, Get, Post, Req, UseGuards, UsePipes, ValidationPipe, Request } from '@nestjs/common';
import { JwtAuthGuard } from 'src/auth/jjwt-auth.guard';
import { WordUserDto } from 'src/wordle/dtos/worduser.dto';
import { WordleService } from 'src/wordle/service/wordle/wordle.service';

@Controller('wordle')
export class WordleController {
    constructor(private readonly wordleService: WordleService){}


    @Post('user-selected')
    @UsePipes(ValidationPipe)
    @UseGuards(JwtAuthGuard)
    userSelected(@Body() body: WordUserDto, @Request() req){
        return this.wordleService.userSelected(body, parseInt(req.user.id));
    }
}
