import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CurrentWord } from 'src/entities/currentword.entity';
import { UsedWords } from 'src/entities/usedwords.entity';
import { UserWord } from 'src/entities/userword.entity';
import { WordleController } from './controller/wordle/wordle.controller';
import { WordleService } from './service/wordle/wordle.service';

@Module({
    imports: [TypeOrmModule.forFeature([CurrentWord, UsedWords, UserWord])],
  controllers: [WordleController],
  providers: [WordleService]
})
export class WordleModule {}