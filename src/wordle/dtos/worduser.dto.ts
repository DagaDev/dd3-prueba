import { IsEmail, IsNotEmpty, MaxLength, MinLength } from "class-validator";

export class WordUserDto {
  @IsNotEmpty()
  @MinLength(5)
  @MaxLength(5)
  userWord: string;
}