import { Column, Entity, PrimaryGeneratedColumn } from "typeorm";

@Entity()
export class CurrentWord{
    @PrimaryGeneratedColumn({
        type: 'bigint',
        name: 'id'
    })
    id: number;

    @Column({
        name: 'current_word'
    })
    currentWord: string;

    @Column({
        name: 'time',
        type: 'timestamp', 
        default: () => 'CURRENT_TIMESTAMP'
    })
    time: string;


}