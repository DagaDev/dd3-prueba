import { Column, Entity, PrimaryGeneratedColumn } from "typeorm";

@Entity()
export class User{
    @PrimaryGeneratedColumn({
        type: 'bigint',
        name: 'id'
    })
    id: number;

    @Column({
        unique: true,
        name: 'use_word'
    })
    email: string;

    @Column({
        name: 'password'
    })
    password: string;


}