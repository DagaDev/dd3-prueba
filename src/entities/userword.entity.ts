import { Column, Entity, PrimaryGeneratedColumn } from "typeorm";

@Entity()
export class UserWord{
    @PrimaryGeneratedColumn({
        type: 'bigint',
        name: 'id'
    })
    id: number;

    @Column({
        name: 'word'
    })
    word: string;

    @Column({
        name: 'time',
        type: 'timestamp', 
        default: () => 'CURRENT_TIMESTAMP'
    })
    time: string;

    @Column({
        name: 'attempts',
        default: 0
    })
    attempts: number

    @Column({
        name: 'userId'
    })
    userId: number;

    @Column({
        name: 'win',
        default: false
    })
    win: boolean;


}