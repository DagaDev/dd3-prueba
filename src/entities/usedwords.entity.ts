import { Column, Entity, PrimaryGeneratedColumn } from "typeorm";

@Entity()
export class UsedWords{
    @PrimaryGeneratedColumn({
        type: 'bigint',
        name: 'id'
    })
    id: number;

    @Column({
        unique: true,
        name: 'use_word'
    })
    usedWord: string;

    @Column({
        name: 'time',
        type: 'timestamp', 
        default: () => 'CURRENT_TIMESTAMP'
    })
    time: string;

}