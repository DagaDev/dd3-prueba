import { IsEmail, IsNotEmpty, MaxLength, MinLength } from "class-validator";

export class UserDto {
  @IsNotEmpty()
  email: string;

  @IsNotEmpty()
  @MinLength(6)
  @MaxLength(6)
  password: string;
}