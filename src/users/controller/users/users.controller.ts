import { Controller, Get, Param, UseGuards, UsePipes, ValidationPipe } from '@nestjs/common';
import { UsersService } from 'src/users/service/users/users.service';
import {Post, Body} from '@nestjs/common';
import { UserDto } from 'src/users/dtos/user.dto';
import { JwtAuthGuard } from 'src/auth/jjwt-auth.guard';

@Controller('users')
export class UsersController {
    constructor(
        private readonly userService: UsersService
    ){}

    @Post('register')
    @UsePipes(ValidationPipe)
    async register(@Body() body: UserDto){
        return this.userService.register(body);
    }

    @Post('login')
    @UsePipes(ValidationPipe)
    async login(@Body() body: UserDto){
        return this.userService.login(body);
    }

    @Get('game-information/:id')
    async gameInformation(@Param('id')id : string){
        return this.userService.gameInformation(parseInt(id));
    }

    @Get('the-best-players')
    async theBest(){
        return this.userService.theBest();
    }

    @Get('the-best-words')
    async theBestWords(){
        return this.userService.theBestWords();
    }
}
