import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { JwtModule } from '@nestjs/jwt';
import { TypeOrmModule } from '@nestjs/typeorm';
import { JwtStrategy } from 'src/auth/jwt.strategy';
import { User } from 'src/entities/user.entity';
import { UserWord } from 'src/entities/userword.entity';
import { CryptService } from 'src/services/crypt-password';
import { UsersController } from './controller/users/users.controller';
import { UsersService } from './service/users/users.service';

@Module({
    imports: [
        ConfigModule.forRoot({isGlobal: true}),
        TypeOrmModule.forFeature([User, UserWord]),
        JwtModule.register({
            secret: process.env.SECRET_JWT,
            signOptions: { expiresIn: '3600s' },
        }),
    ],
  controllers: [UsersController],
  providers: [UsersService, CryptService, JwtStrategy]
})
export class UsersModule {}
