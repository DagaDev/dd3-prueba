import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { InjectRepository } from '@nestjs/typeorm';
import { User } from 'src/entities/user.entity';
import { UserWord } from 'src/entities/userword.entity';
import { CryptService } from 'src/services/crypt-password';
import { UserDto } from 'src/users/dtos/user.dto';
import { Repository } from 'typeorm';

@Injectable()
export class UsersService {
    constructor(
        private readonly cryptService: CryptService,
        @InjectRepository(User) private readonly userRepository: Repository<User>,
        @InjectRepository(UserWord) private readonly userWord: Repository<UserWord>,
        private jwtService: JwtService
    ){}

    async login(body: UserDto){
        let { email, password } = body;
        let user = await this.userRepository.findOne({where: {email}});
        if(!user)
            throw new HttpException('User/password not found', HttpStatus.NOT_FOUND);
            
        let compare = await this.cryptService.compare(password, user.password);
        if(!compare)
            throw new HttpException('User/password not found', HttpStatus.NOT_FOUND);

        const payload = { email: user.email, id: user.id };
        return {
            access_token: this.jwtService.sign(payload),
        };
    }

    async gameInformation(id: number){
        const user = await this.userRepository.findOne({where: {id: id}});
        if(!user)
            throw new HttpException('User not found user', HttpStatus.NOT_FOUND)

        const userWord = await this.userWord.find({where: {userId: id}});
        const victories = userWord.filter(value => value.win === true);

        return {
            victories: victories.length,
            gamesPlayed: userWord.length
        }
    }

    async theBest(){
        const userWord = await this.userWord.query('select "userId", count(win) AS victories from public.user_word where win=true GROUP BY "userId" ORDER BY victories desc limit 10;')
        return userWord;
    }

    async theBestWords(){
        const userWord = await this.userWord.query('select "word", count(word) as correct from public.user_word group by word order by correct desc;');
        return userWord;
        
    }
    
    
    async register(body: UserDto){
        let { email, password } = body;
        let user = await this.userRepository.findOne({where: {email}});
        if(user)
            throw new HttpException('registered user', HttpStatus.CONFLICT)
        let hash = await this.cryptService.hash(password);
        await this.userRepository.save({
            email, 
            password: hash
        });
        return {
            message: 'registered user'
        }

    }
}
