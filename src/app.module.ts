import { Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CurrentWord } from './entities/currentword.entity';
import { UsedWords } from './entities/usedwords.entity';
import { WordleModule } from './wordle/wordle.module';
import { UsersModule } from './users/users.module';
import { User } from './entities/user.entity';
import { WordleService } from './wordle/service/wordle/wordle.service';
import { UserWord } from './entities/userword.entity';
@Module({
    imports: [
        ConfigModule.forRoot({isGlobal: true}),
        TypeOrmModule.forRootAsync({
            imports: [ConfigModule],
            useFactory:(configService: ConfigService) =>({
                type: 'postgres',
                host: configService.get('DB_HOST'),
                port: configService.get<number>('DB_PORT'),
                username: configService.get('DB_USERNAME'),
                password: configService.get('DB_PASSWORD'),
                database: configService.get('DB_NAME'),
                entities: [CurrentWord, UsedWords, User, UserWord],
                synchronize: true
            }),
            inject: [ConfigService]
        }),
        WordleModule,
        UsersModule,
        TypeOrmModule.forFeature([CurrentWord, UsedWords, UserWord])
    ],
    controllers: [],
    providers: [WordleService],
})
export class AppModule {
    constructor(private wordService: WordleService){
        wordService.getListWords().then(() => {
            wordService.initial();
            wordService.cronNode()
        });
    }
}
